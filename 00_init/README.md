---

K8s Workshop
---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is k8s ?
- Who needs k8s ?
- How k8s works ?
- How to manage k8s in various scenarios ?


### Who Is This course for ?

- Junior/senior sysadmins who have some knowledge of containers and want to extend it
- For junior/senior developers who willing to upgrade their infrastructure knowledge

- Experienced ops who need refresher


---

# Course Topics

- Intro
- K8s basics
- K8s networking
- K8s storage
- K8s container expose
- K8s scaling
- K8s rolling updates
- K8s ingress
- K8s helm deployments

---
# About Me
<!-- <img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;"> -->
![bg right:20% contain](../99_misc/.img/me.jpg)

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---
# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourselves:

- Name and Last-name
- What do you do ? Not only job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Containers: Docker / Docker-Compose / K8s
    - CI/CD: Jenkins / GitLab / Github / Gitea / Bitbucket
    - Scripting/Programming : Bash/ PowerShell / Python/ Javascript/ Go/ C/ C++
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---

# History 101

Kubernetes, ancient Greek'steersman, navigator' or 'guide' was announced by Google on June 6, 2014. The project was conceived and created by Google employees Joe Beda, Brendan Burns, and Craig McLuckie. Others at Google soon joined to help build the project including Ville Aikas, Dawn Chen, Brian Grant, Tim Hockin, and Daniel Smith.Other companies such as Red Hat and CoreOS joined the effort soon after, with notable contributors such as Clayton Coleman and Kelsey Hightower

---

# History 101
The design and development of Kubernetes was inspired by Google's Borg cluster manager and based on Promise Theory. Many of its top contributors had previously worked on Borg; they codenamed Kubernetes "Project 7" after the Star Trek ex-Borg character Seven of Nine and gave its logo a seven-spoked ship's wheel (designed by Tim Hockin). Unlike Borg, which was written in C++,Kubernetes is written in the Go language.

---

# History 101
Kubernetes was announced in June, 2014 and version 1.0 was released on July 21, 2015. Google worked with the Linux Foundation to form the Cloud Native Computing Foundation (CNCF) and offered Kubernetes as the seed technology.

---

# History 101

Google was already offering a managed Kubernetes service, GKE, and Red Hat was supporting Kubernetes as part of OpenShift since the inception of the Kubernetes project in 2014. In 2017, the principal competitors rallied around Kubernetes and announced adding native support for it:

- VMware (proponent of Pivotal Cloud Foundry) in August,
- Mesosphere, Inc. (proponent of Marathon and Mesos) in September,
- Docker, Inc. (proponent of Docker) in October,
- Microsoft Azure also in October,
- AWS announced support for Kubernetes via the Elastic Kubernetes Service (EKS) in November.

---

# History 101
On March 6, 2018, Kubernetes Project reached ninth place in the list of GitHub projects by the number of commits, and second place in authors and issues, after the Linux kernel.
