#!/usr/bin/env bash 
##############################################
# Created by: Silent-Mobius AKA Alex M. Schapelle
# Purpose: Env setup for learning k8s basics
# 
#
#
NULL=/dev/null
##############################################


function main(){
    if [[ $EUID != 0 ]];then
        echo "[+] Run with sudo or become root"
        exit 1
    else
        if which curl > $NULL 2>&1; then
            curl -L get.docker.com | bash
            curl -L get.k3s.io | bash 
        elif which wget > $NULL 2>&1;then
            wget -O- get.k3s.io | bash
        fi
    fi
}


#######
# Main  - _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _- _
######
main "$@"