---

K8s basics

---

# K8s basics

### What is kubernetes ?

- Kubernetes is a container management and orchestration system
- It runs and manages containerized applications on a cluster

---
 # K8s basics

### What does it really mean ?

- Generally speaking:
    - Theoretically it means that you don't need to do a thing, just deploy your container and K8s will run it, connect it, configure it, manage it and scale it.
    - Practically it means that you need to plan how and what your application need to do for it to work, and translate those ideas into YAML format.

> YAML is JSON for lazy people or python developers -> We'll get there

---

# K8s basics
 
### Yes, but what does K8s really does?
- Let's use [one of our applications that we have developed](https://gitlab.com/vaiolabs-io)
    - Usually we have 2 to 4 container application that are managed with docker-compose
        - FrontEnd/UI container
        - API BackEnd
        - Database ( which we will kee out of K8s for now)
    - Let's see how we can learn the use of K8s from here.
- But before that

---

# K8s basics

### K8s Architecture

<img src="../99_misc/.img/k8s-arch1.png" alt="k8s arch" style="float:right;width:400px;"

---

# K8s basics

### Architecture

- Just joking, let's take it slowly:
    - The main parts 

<img src="../99_misc/.img/k8s-arch2.png" alt="k8s arch" style="float:right;width:400px;"

---

# K8s basics

### Setup

For practice sake and easy of use we'll use lightweight k8s distribution named k3s, previous known as rancher. The idea of rancher being k8s with only with stable features and without `alpha` or `beta` features in

```sh
curl -L https://k3s.io | sh -
```

---

# K8s basics

# Pods

A `Pod`` is the basic execution unit of a Kubernetes application. It is a collection of containers that are deployed together on the same host. Pods in a Kubernetes cluster can be used in two ways:

- Pods that run a single container: This is the most recommended way of using a Pod. Kubernetes manages the pods instead of directly managing the containers. 
- Pods that run multiple containers that need to work together: In this model, a pod can have multiple containers that are tightly coupled to share resources. 

As stated above that a Pod can contain multiple containers, it is always recommended to have a single container when possible. Grouping multiple containers in a single Pod is a relatively advanced use case and it adds architectural complexity. You should use this pattern only in specific instances in which your containers are tightly coupled, for example, getting logs out of container with other container for shipping logs

---


# K8s basics

### Pods

##### The 5 stages in a pod lifecycle

- **Pending**: The Pod has been accepted by the Kubernetes system, but one or more of the Container images has not been created. This includes time before being scheduled as well as time spent downloading images over the network, which could take a while.
- **Running**: The Pod has been bound to a node, and all of the Containers have been created. At least one Container is still running, or is in the process of starting or restarting.
- **Succeeded**: All Containers in the Pod have terminated in success, and will not be restarted.
- **Failed**: All Containers in the Pod have terminated, and at least one Container has terminated in failure. That is, the Container either exited with non-zero status or was terminated by the system.
- **Unknown**: For some reason, the state of the Pod could not be obtained, typically due to an error in communicating with the host of the Pod.

---

# K8s basics

### Pods

##### Deploying Pods
To create our first pod, let's just create a new directory to create first yaml file.
Use the following command to list the pods in the __default namespace__. 

```sh
sudo kubectl get pods
```
> Note: Since this is will be our first pod on the Cluster, you will not see any pod in the __default namespace__.

Once you have Nodes available in the cluster you are ready to create your first pod.

Create a file "my-first-pod.yml" with the following block of code
```sh
vim my-first-pod.yml
 ```
 and save the manifest written below
```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: myfirstpod
  labels:
    app: web
spec:
  containers:
    - name: myfirstcontainer
      image: nginx
      ports:
        - containerPort: 80

```

Some explanation: 
- **apiVersion**: APIVersion defines the versioned schema of this representation of an object.
- **kind**: Kind of object you want to create. Here it is pod since we are creating a pod.
- **name**: Name must be unique within a namespace. 
- **labels**: Map of string keys and values that can be used to organize and categorize objects
- **spec**: Specification of the desired behavior of the pod.

Now you are ready to create your pod using the following command and check its status

```sh
sudo kubectl apply -f my-first-pod.yml 
sudo kubectl get pods
```

To test whether the Pod is running, use the following command. This runs a command inside our pod (Note: It is similar to running docker exec.)
```sh
sudo kubectl exec myfirstpod — service nginx status
```
---

# K8s basics

### Pods

##### Deleting  Pods
In case you no longer require the pod you can delete it:
```sh
sudo kubectl delete pods myfirstpod
```
or 
```sh
sudo kubectl delete -f my-first-pod.yml
```
At last to verify if the pod has been deleted :
```sh
sudo kubectl get pods
```

---

# K8s basics

### Deployment

A Deployment provides declarative updates for Pods and ReplicaSets. We describe a desired state in the Deployment and the Deployment Controller changes the actual state to the desired state at a controlled rate.

We can create and manage a Deployment by using the  same `kubectl` utility.

There are 3 stages in a deployment lifecycle:

- **Progressing**: Kubernetes marks a Deployment as progressing when the Deployment creates a new ReplicaSet, the Deployment is scaling up its newest ReplicaSet or scaling down its older ReplicaSet or new Pods become ready or available 
- **Complete**: Kubernetes marks a Deployment as complete when all of the replicas associated with the Deployment have been updated, are available, and no old replicas for the Deployment are running.
- **Failed**: This can occur due to insufficient quota, readiness probe failures, image pull errors or insufficient permissions.

---

# K8s basics

### Deployment

Create a file "my-first-deployment.yml" with the following block of code

vim my-first-deployment.yml

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80

Here,

    apiVersion: APIVersion defines the versioned schema of this representation of an object.
    kind: The kind of object you want to create like Deployment, Service, Configmap, and more.
    name: The name must be unique within a namespace. 
    labels: Map of string keys and values that can be used to organize and categorize objects
    spec: Specification of the desired behavior of the Deployment.
    replicas: Number of desired pods.
    selector: Label selector for pods. Existing ReplicaSets whose pods are selected by this will be the ones affected by this deployment. It must match the pod template’s labels.

Now you are ready to create your deployment using the following commands.

sudo kubectl apply -f my-first-deployment.yml
sudo kubectl  get deployments